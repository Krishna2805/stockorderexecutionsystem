﻿namespace StockOrderExecutionSystem.Tests.FileReader.Tests
{
    using NUnit.Framework;
    using StockOrderExecutionSystem.FileReader;

    [TestFixture]
    class ReadCsvTests
    {
        private IReadCsv readText; //Real world solutions have moq in place.

        [SetUp]
        public void SetUp()
        {
            readText = new ReadCsv();
        }
        [Test]
        public void Verify_file_path_to_return_values()
        {
            //Arrange - Act - Assert

            string filepath = FilePath.filePath();

            var result = readText.Read(filepath);

            Assert.IsNotEmpty(result);
        }
    }
}
