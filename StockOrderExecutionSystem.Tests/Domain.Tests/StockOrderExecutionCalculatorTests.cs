﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StockOrderExecutionSystem.Adapter;
using StockOrderExecutionSystem.Domain;
using StockOrderExecutionSystem.FileReader;

namespace StockOrderExecutionSystem.Tests.Domain.Tests
{
    internal class StockOrderExecutionCalculatorTests
    {
        [TestFixture]
        private class StockOrderAdapterTests
        {
            private StockOrderExecutionCalculator stockOrderExecutionCalculator;
            private IStockOrdersAdapter stockOrdersAdapter;

            private List<string> input = new List<string>()
            {
                "Stock Id,Side,Company,Quantity",
                "1,Buy,ABC,10",
                "2,Sell,XYZ,15",
                "3,Sell,ABC,13",
                "4,Buy,XYZ,10",
                "5,Buy,XYZ,8"
            };

            [SetUp]
            public void SetUp()
            {
                stockOrderExecutionCalculator = new StockOrderExecutionCalculator(new ReadCsv(),
                    new StockOrdersAdapter());
                stockOrdersAdapter = new StockOrdersAdapter();
            }

            [Test]
            public void Verify_Stock_Order()
            {
                //needs refactoring in the calculator
                var list = stockOrdersAdapter.Connect(input);
                stockOrderExecutionCalculator.CalculateBuyOrSell(list);
                Assert.IsNotEmpty(list);
            }

        }
    }
}