﻿namespace StockOrderExecutionSystem.Tests.Adapter.Tests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using StockOrderExecutionSystem.Adapter;

    [TestFixture]
    class StockOrderAdapterTests
    {
        private IStockOrdersAdapter stockOrdersAdapter;

        [SetUp]
        public void SetUp()
        {
            stockOrdersAdapter=new StockOrdersAdapter();
            
        }

        [Test]
        public void Verify_adapter_for__return_values()
        {
            var input = new List<string>() { "Stock Id,Side,Company,Quantity", "1,Buy,ABC,10", "2,Sell,XYZ,15", "3,Sell,ABC,13", "4,Buy,XYZ,10", "5,Buy,XYZ,8" };
            var output=stockOrdersAdapter.Connect(input);
            Assert.AreEqual("ABC",output[0].Company);
        }
    }
}
