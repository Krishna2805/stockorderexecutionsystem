﻿using StockOrderExecutionSystem.Adapter;
using StockOrderExecutionSystem.Common;
using StockOrderExecutionSystem.FileReader;
using StockOrderExecutionSystem.Model;

namespace StockOrderExecutionSystem.Domain
{
    using System;
    using System.Collections.Generic;

    public class StockOrderExecutionCalculator
    {
        public IReadCsv ReadCsv;
        public IStockOrdersAdapter StockOrdersAdapter;
        public List<string> Inputs;

       public StockOrderExecutionCalculator(IReadCsv readCsv,IStockOrdersAdapter stockOrdersAdapter)
        {
            ReadCsv = readCsv;
           this.StockOrdersAdapter = stockOrdersAdapter;
        }

        public void CalculateStockOrder( List<string> input)
        {
            List<StockOrders> inputs = StockOrdersAdapter.Connect(input);
            var dictionary = new Dictionary<string, int>();

            var output = new List<StockOrders>();

            //2. Determine if buy or sell
            CalculateBuyOrSell(inputs);


            //3. add companies to the list
            AddCompaniesToList(inputs, dictionary);

            //4. calculation
            CalculateStocks(inputs, dictionary, output);

            //5.print
            Print(output);

            Console.ReadLine();
        }

        private void Print(IEnumerable<StockOrders> output)
        {
            Console.WriteLine("Side Company Quantity RemainingQuantity Status");
            foreach (var formatse in output)
            {
                Console.WriteLine("{0}\t {1}\t {2}\t {3}\t\t {4}", formatse.Side, formatse.Company, Math.Abs(formatse.Quantity),
                    Math.Abs(formatse.RemainingQuantity), formatse.Status);
            }
        }

        public void CalculateStocks(IEnumerable<StockOrders> inputs, Dictionary<string, int> dictionary, List<StockOrders> output)
        {
            foreach (StockOrders t in inputs)
            {
                int company = dictionary[t.Company];
                t.RemainingQuantity = t.Quantity + company;
                dictionary[t.Company] = t.RemainingQuantity;
                if ((t.Quantity == t.RemainingQuantity) || (t.Side.Equals(Side.Buy) && t.RemainingQuantity < 0))
                {
                    t.RemainingQuantity = 0;
                }
                t.Status = t.RemainingQuantity == 0 ? Status.Closed : Status.Open;

                output.Add(t);
            }
        }

        public void AddCompaniesToList(IEnumerable<StockOrders> inputs, Dictionary<string, int> dictionary)
        {
            foreach (StockOrders t in inputs)
            {
                if (!dictionary.ContainsKey(t.Company))
                {
                    dictionary.Add(t.Company, t.RemainingQuantity);
                }
            }
        }

        public void CalculateBuyOrSell(IEnumerable<StockOrders> inputs)
        {

            foreach (var inputString in inputs)
            {
                if (inputString.Side.Equals(Side.Sell))
                {
                    inputString.Quantity = inputString.Quantity*-1;
                }
            }
        }
    }
}
