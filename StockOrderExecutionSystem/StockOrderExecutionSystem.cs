﻿using StockOrderExecutionSystem.Adapter;
using StockOrderExecutionSystem.Domain;
using StockOrderExecutionSystem.FileReader;

namespace StockOrderExecutionSystem
{
    
    class StockOrderExecutionSystem
    {

        static void Main(string[] args)
        {
                var readCsv = new ReadCsv();
                var stockOrderExecutinCalculator = new StockOrderExecutionCalculator(readCsv,new StockOrdersAdapter());
                stockOrderExecutinCalculator.CalculateStockOrder(readCsv.Read(args[0]));

        }
    }
}
