﻿namespace StockOrderExecutionSystem.Model
{
    using System;

    public class StockOrders
    {
        public Enum Side { get; set; }

        public string Company { get; set; }

        public int Quantity { get; set; }

        public int RemainingQuantity { get; set; }

        public Enum Status { get; set; }
    }
}
