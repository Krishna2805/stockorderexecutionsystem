﻿namespace StockOrderExecutionSystem.Common
{
    using System;

    public class Validations
    {
        public static bool IsValid(string input, int lineNumber)
        {
            string[] inputSplit = input.Split(',');
            foreach (var inputs in inputSplit)
            {
                while (inputs == string.Empty)
                {
                    Console.WriteLine("line number {0} from input has empty elements",lineNumber);
                    return false;
                } 
            }
           
            

            return true;
        }
    }
}
