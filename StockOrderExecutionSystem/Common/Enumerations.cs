﻿namespace StockOrderExecutionSystem.Common
{
    public enum Side
    {
        Buy,
        Sell
    };

    public enum Status
    {
        Open,
        Closed
    };
}
