﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockOrderExecutionSystem.Common;
using StockOrderExecutionSystem.Model;

namespace StockOrderExecutionSystem.Adapter 
{
    public class StockOrdersAdapter : IStockOrdersAdapter
    {
        public List<StockOrders> StockOrders=new List<StockOrders>(); 
        public List<StockOrders> Connect(List<string> input)
        {
            input.RemoveAt(0);
            foreach (var singleInput in input)
            {
                string[] inputArray = singleInput.Split(',');
                var side = (Side)Enum.Parse(typeof(Side), inputArray[1], true);
                var company = inputArray[2];
                var quantity = Convert.ToInt32(inputArray[3]);
                StockOrders.Add(new StockOrders() { Side = side, Company = company, Quantity = quantity });

            }
            return StockOrders;
        }
    }
}
