﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using StockOrderExecutionSystem.Model;

namespace StockOrderExecutionSystem.Adapter
{
    public interface IStockOrdersAdapter
    {
        List<StockOrders> Connect(List<string> input);
    }
}
