﻿namespace StockOrderExecutionSystem.FileReader
{
    using System.Collections.Generic;

    public interface IReadCsv
    {
        List<string> Read(string path);
    }
}
