﻿using StockOrderExecutionSystem.Common;

namespace StockOrderExecutionSystem.FileReader
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ReadCsv : IReadCsv
    {
        public List<string> Read(string path)
        {
            var inputs = new List<string>();
            int lineNumber = 0;
            try
            {
                using (var sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (Validations.IsValid(line, lineNumber)) inputs.Add(line);
                        lineNumber++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return inputs;

        }

    }
}
